package org.eshae.eshael.event;

import java.util.EventObject;

import org.eshae.eshael.ludo.Coin;

public class LudoCutEvent  extends EventObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Coin coin;
	
	public LudoCutEvent(Object source) {
		super(source);
		// TODO Auto-generated constructor stub
	}

	public Coin getCoin() {
		return coin;
	}

	public void setCoin(Coin coin) {
		this.coin = coin;
	}
	

	
}
